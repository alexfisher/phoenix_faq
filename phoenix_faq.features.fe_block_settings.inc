<?php
/**
 * @file
 * phoenix_faq.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function phoenix_faq_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-faq-back_button'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'faq-back_button',
    'module' => 'views',
    'node_types' => array(
      0 => 'faq',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'compro_theme',
        'weight' => -35,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
